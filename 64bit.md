<table width="100%" bgcolor="#ddffdd">

<tbody>

<tr>

<th>last updated: 2017-10-07</th>

</tr>

</tbody>

</table>

Back to the [Gaia-SINS](http://www.gaia-gis.it/gaia-sins/index.html) home page

### Index of contents

*   [the MinGW64 + MSYS2 environment](#env)
*   [Step 1) building libiconv](#libiconv)
*   [Step 2) building libz](#libz)
*   [Step 3) building libjpeg](#libjpeg)
*   [Step 4) building libpng](#libpng)
*   [Step 5) building XZ (liblzma)](#liblzma)
*   [Step 6) building libtiff](#libtiff)
*   [Step 7) building libproj](#libproj)
*   [Step 8) building libgeotiff](#libgeotiff)
*   [Step 9) building libgeos](#libgeos)
*   [Step 10) building libexpat](#libexpat)
*   [Step 11) building FreeXL](#freexl)
*   [Step 12) building ReadOSM](#readosm)
*   [Step 13) building libsqlite](#libsqlite)
*   [Step 14) building OpenSSL](#open-ssl)
*   [Step 15) building PostgreSQL](#postgres)
*   [Step 16) building libxml2](#libxml2)
*   [Step 17) building librttopo](#librttopo)
*   [Step 18) building libspatialite](#libspatialite)
*   [Step 19) building spatialite-tools](#spatialite-tools)
*   [Step 20) building wxWidgets MSW](#wxWidgets)
*   [Step 21) building libfreetype](#libfreetype)
*   [Step 22) building libfontconfig](#libfontconfig)
*   [Step 23) building libpixman](#libpixman)
*   [Step 24) building libcairo](#libcairo)
*   [Step 25) building giflib](#giflib)
*   [Step 26) building libwebp](#libwebp)
*   [Step 27) building CharLS](#CharLS)
*   [Step 28) building lcms2](#lcms2)
*   [Step 29) building OpenJpeg-2](#OpenJpeg)
*   [Step 30) building libcurl](#libcurl)
*   [Step 31) building librasterlite2](#librasterlite2)
*   [Step 32) building spatialite_gui](#spatialite-gui)
*   [Step 33) building LibreWMS](#librewms)
*   [Step 34) building minizip](#minizip)
*   [Step 35) building libgci](#libcgi)
*   [Step 36) building DataSeltzer](#dataseltzer)
*   [Step 37) building GDAL](#gdal)
*   [Step 38) building fossil](#fossil)

* * *

### <a name="env">the MinGW64 + MSYS2 environment</a>

**MinGW64** is an _open source_ C/C++ compiler for WIndows platforms based on the popular **gcc** available on Linux and on many other Operating Systems; Mingw64 can create both 32 and 64 bit binary executables.
**MSYS2** is a command shell supporting a minimalistic Linux-like environment on Windows.
Using both them you can build standard open source software [_originally developed for Linux_] under Windows as well, simply using the _classic_ build tool-chain, as in:

**./configure**<br>
**make**<br>
**make install**<br>

Quite obviously, Windows isn't exactly the same as Linux, so you cannot expect to get always an easy and painless build.
May well be you have to adapt something here and there in order to get a successful build under Windows.
The following notes are aimed exactly to this goal: allow you to avoid wasting your time while fighting against oddities that can quite easily solved.

<table width="100%" bgcolor="#ddffdd" cellpadding="16">

<tbody>

<tr>

<th>**Important notice**: all the following instructions require using the most recent versions of the compiler (i.e. **gcc 6.3.0**).

In order to check the actual version you are using, you simply have to execute the following command: **gcc --version**</th>

</tr>

</tbody>

</table>

The most recent versions of MinGW64 and MSYS2 are reasonably easy to install; just follow the instructions below.

### Installing MSYS2

1.  download the MSYS2 intaller from [here](http://www.msys2.org/)
2.  start the installer: I suggest you to set **C:/dev/msys64** as the base installation path.
3.  once MSYS2 has been installed you must complete several further steps.
    Please follow the instructions you can find [here](://github.com/orlp/dev-on-windows/wiki/Installing-GCC--&-MSYS2)
4.  **Note**: MSYS2 is a common shell environment supporting both the 32 bit and the 64 bit compilers, so you have to perform this step just once.

### Familiarizing with **pacman**<br>

After properly installing MSYS2 you'll be able to use **pacman** in order to complete your installation.
Pacman is a package manager similar to Red Hat's **yum** and Debian's **apt-get**, and will make really easy the task to prepare a complete build environment.

1.  Start an MSYS2 shell.
    Just click the icon of one between **msys2.exe**, **mingw32i.exe** or **mingw64.exe** you'll find into the **C:\dev\msys64** folder (the one or the other is not relevant in this context).
2.  Then execute the following commands from the shell:
    *   **pacman -S --needed base-devel**<br>
        This will install **make**, **pkg-config** and many others useful tools.
    *   **pacman -S --needed mingw-w64-i686-toolchain**<br>
        This will install the **32 bit** compiler.
    *   **pacman -S --needed mingw-w64-x86_64-toolchain**<br>
        This will install the <n>64 bit compiler.</n>
    *   **pacman -S --needed mingw-w64-i686-cmake mingw-w64-x86_64-cmake**<br>
        This will install **CMake** for both 32 and 64 bit.
3.  all right: you've now completed your MinGW64 + MSYS2 installation, and you are ready to start building your Windows binary executables.

**Note**: may well be you'll discover before or after that some tool is still missing. You can easily install it when required.
Example: the well known **vim** text editor is still missing; you can easily install it by executing: **pacman -S vim**<br>
Hint: you can easily check all packages declaring a matching name and if they are already installed or not by executing: **pacman -Ss package-name**:

### Understanding the two build environments: **mingw32** and **mingw64**<br>

*   the **mingw32.exe** shell will automatically target the 32 bit compiler. So you'll simply have to launch **ming32** in order to build any 32 bit executable.
*   the **C:\dev\msys64\mingw32** folder is the base directory for all 32 bit software. It will be internally mapped by MSYS2 as **/mingw32**<br>
*   the **mingw64.exe** shell will automatically target the 64 bit compiler. So you'll simply have to launch **ming64** in order to build any 64 bit executable.
*   the **C:\dev\msys64\mingw64** folder is the base directory for all 64 bit software. It will be internally mapped by MSYS2 as **/mingw64**<br>

In order to mantain a properly ordered filesystem layout, clearly separating 32 bit and 64 bit components we can't any longer adopt the classic **/usr/local** target as the standard destination where to install our custom builds.

*   **/mingw32/local**<br>
    this will be the directory where to install all 32 bit builds.
*   **/mingw64/local**<br>
    and this will be the directory where to install all 64 bit builds.

### Very important notice

The standard configuration adopted by **MinGW64** forbids to effectively link **libpthread** as a static library.
You are required to manually apply the following small patch in order to avoid this issue.

*   this patch must be applied twice, once for the 32 bit compiler and once for the 64 bit compiler.
*   you must edit the **/mingw32/i686-w64-mingw32/include/pthread.h** header file for the 32 bit compiler.
*   and/or you must edit the **/mingw64/x86_64-w64-mingw32/include/pthread.h** header file for the 64 bit compiler.

Near line 85:

```
  #if defined DLL_EXPORT
  #ifdef IN_WINPTHREAD
  #define WINPTHREAD_API __declspec(dllexport)
  #else
< #define WINPTHREAD_API __declspec(dllimport)
> #define WINPTHREAD_API
  #endif
  #else
  #define WINPTHREAD_API
  #endif
```

* * *

### <a name="libiconv">Step 1) building libiconv</a>

**libiconv** is the standard **GNU** library supporting _locale charsets_.
Required by: **libspatialite**, **spatialite-tools**<br>

Building under Windows is not too much difficult.

*   download [libiconv-1.15.tar.gz](http://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.15.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd libiconv-1.15**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

Anyway, this will simply build and install the DLL: a further step is required in order to get the _static library_ as well.

**make distclean**<br>
**./configure --prefix=/mingw64/local --disable-shared --enable-static**<br>
**make**<br>
**make install-strip**<br>

Now you've built and installed both the _static library_ and the DLL.
However the above process has installed badly misconfigured **libcharset.la** and **libiconv.la** files
(which are required to build other libraries in the following steps).
So in order to get a properly configured **libiconv** you have to accomplish a further operation:

*   download [libiconv.la](http://www.gaia-gis.it/gaia-sins/mingw64/libiconv.la) and [libcharset.la](http://www.gaia-gis.it/gaia-sins/mingw64/libcharset.la)
*   then copy both files:
    **cp libiconv.la libcharset.la /mingw64/local/lib**<br>

* * *

### <a name="libz">Step 2) building libz</a>

**libz** is a popular library implementing _Deflate_, i.e. the compression algorithm used by **gzip** and **Zip**.
Depends on: **nothing**<br>
Required by: **libpng**, **libtiff**, ...

Building under Windows is quite easy, but requires to pay some attenction.

*   download the latest sources: [zlib1211.zip](http://zlib.net/zlib1211.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd zlib-1.2.11**<br>
**export "INCLUDE_PATH=/mingw64/local/include"**<br>
**export "LIBRARY_PATH=/mingw64/local/lib"**<br>
**export "BINARY_PATH=/mingw64/local/bin"**<br>
**make -f win32/Makefile.gcc**<br>
**make install -f win32/Makefile.gcc**<br>
**cp libz.dll.a /mingw64/local/lib**<br>
**cp zlib1.dll /mingw64/local/bin**<br>

All this will build and install both the _static library_ and the DLL as well.
Anyway this process will not generate the **libz.la** file (which is required to build **libtiff** in one of the following steps.
So in order to get a fully installed **libz** you have to accomplish a further operation:

*   download [libz.la](http://www.gaia-gis.it/gaia-sins/mingw64/libz.la)
*   and then copy this file: **cp libz.la /mingw64/local/lib**<br>

* * *

### <a name="libjpeg">Step 3) building libjpeg</a>

**libjpeg** is a popular library supporting the **JPEG** image compression.
Depends on: **nothing**<br>
Required by: **libtiff**, **librasterlite2**<br>

Building under Windows is absolutely a plain and easy task.

*   download the latest sources: [libjpeg-turbo-1.5.2.tar.gz](http://sourceforge.net/projects/libjpeg-turbo/files/1.5.2/libjpeg-turbo-1.5.2.tar.gz/download)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd libjpeg-turbo-1.5.2**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libpng">Step 4) building libpng</a>

**libpng** is a popular library supporting the **PNG** image compression.
Depends on: **libz**<br>
Required by: **librasterlite2**<br>

Building under Windows is absolutely a plain and easy task.

*   download the latest sources: [libpng-1.6.32.tar.gz](http://prdownloads.sourceforge.net/libpng/libpng-1.6.32.tar.gz?download)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd libpng-1.6.32**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

<u>Important notice</u>: you have to properly set the shell environment in order to retrieve the already installed **libz**; this is the duty of the two above <u>export</u> directives.

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="liblzma">Step 5) building XZ (liblzma)</a>

**liblzma** is a popular library supporting the **LZMA** advanced compression (it's a component of the XZ package).
Depends on: **nothing**<br>
Required by: **libtiff**<br>

Building under Windows is absolutely a plain and easy task.

*   download the latest sources: [xz-5.2.3.tar.gz](http://tukaani.org/xz/xz-5.2.3.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   then open an MSYS2 shell (**mingw64.exe**)

**cd xz-5.2.3**<br>
**./configure --prefix=/mingw64/local --with-libiconv-prefix=/mingw64/local --with-libintl-prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libtiff">Step 6) building libtiff</a>

**libtiff** is a popular library supporting the **TIFF** image format.
Depends on: **libz**, **liblzma**, **libjpeg**<br>
Required by: **librasterlite2**<br>

Building under Windows is absolutely a plain and easy task.

*   download the latest sources: [tiff-4.0.8.zip](http://download.osgeo.org/libtiff/tiff-4.0.8.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd tiff-4.0.8**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

<u>Important notice</u>: you have to properly set the shell environment in order to retrieve the already installed **libz**; this is the duty of the two above <u>export</u> directives.

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libproj">Step 7) building libproj</a>

**libproj** is a library supporting coordinate's transformation between different Reference Systems [**PROJ.4**]
Depends on: **nothing**<br>
Required by: **libgeotiff**, **libspatialite**, **spatialite-tools**<br>

Building under Windows is an easy task.

*   download the latest sources: [proj-4.9.3.zip](http://download.osgeo.org/proj/proj-4.9.3.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   then open an MSYS2 shell (**mingw64.exe**)

**cd proj-4.9.3**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libgeotiff">Step 8) building libgeotiff</a>

**libgeotiff** is a library supporting the **GeoTIFF** raster format
Depends on: **libtiff**, **libproj**<br>
Required by: **librasterlite2**<br>

Building under Windows is an easy task.

*   download the latest sources: [libgeotiff-1.4.2.zip](http://download.osgeo.org/geotiff/libgeotiff/libgeotiff-1.4.2.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

<u>Important notice</u>: there is a bug in the standard build scripts forbidding to create a Windows DLL using MinGW64 + MSYS2.
So you have to manually apply the following patch to circumvent this issue.
(many thanks to Alexey Pavlov for suggesting this patch)
Edit the **Makefile.in** file as follows:

```
570c570
< libgeotiff_la_LDFLAGS = -version-info 3:2:1
---
> libgeotiff_la_LDFLAGS = -version-info 3:2:1 -no-undefined
```

Save and exit; now you are ready to build.

**cd libgeotiff-1.4.2**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local --with-proj --with-libz --with-jpeg --enable-incode-epsg**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libgeos">Step 9) building libgeos</a>

**libgeos** is a library representing a **C++** porting of **JTS** [_Java Topology Suite_].
Depends on: **nothing**<br>
Required by: **libspatialite**, **spatialite-tools**<br>

This library really is an <u>huge and complex</u> piece of software;

*   download the latest sources: [geos-3.6.2.tar.bz2](http://download.osgeo.org/geos/geos-3.6.2.tar.bz2)
*   uncompress this bzip2-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd geos-3.6.2**<br>
**export "CXXFLAGS=-O3"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libexpat">Step 10) building libexpat</a>

**libexpat** is a well known standard library supporting **XML parsing**.
Depends on: **nothing**<br>
Required by: **libfontconfig**, **spatialite-tools**, ...

Building under Windows really is a piece-of-cake.

*   download the latest sources: [expat-2.2.4.tar.bz2](http://sourceforge.net/projects/expat/files/expat/2.2.4/expat-2.2.4.tar.bz2/download)
*   uncompress this bzip2 -file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd expat-2.2.4**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="freexl">Step 11) building FreeXL</a>

**FreeXL** is an utility library implementing read-access for Excel (.xls) spreadsheets
Depends on: **libiconv**<br>
Required by: **spatialite-tools**, **libspatialite**, **spatialite-gui**<br>

Building under Windows is an easy task.

*   download the latest sources: [freexl-1.0.4.zip](http://www.gaia-gis.it/gaia-sins/freexl-1.0.4.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd freexl-1.0.4**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

<u>Please note</u>: if you wish, you can now execute the complete test coverage in order to verify if your build was fully succesful.
This further step is <u>optional</u> and requires a small extra time, but it's strongly suggested anyway.

**make check**<br>

* * *

### <a name="readosm">Step 12) building ReadOSM</a>

**ReadOSM** is an utility library supporting OSM datasets parsing
Depends on: **libz**, **libexpat**<br>
Required by: **spatialite-tools**<br>

Building under Windows is an easy task.

*   download the latest sources: [readosm-1.1.0.zip](http://www.gaia-gis.it/gaia-sins/readosm-1.1.0.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd readosm-1.1.0**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

<u>Please note</u>: if you wish, you can now execute the complete test coverage in order to verify if your build was fully succesful.
This further step is <u>optional</u> and requires a small extra time, but it's strongly suggested anyway.

**make check**<br>

* * *

### <a name="libsqlite">Step 13) building libsqlite</a>

**libsqlite** is the library implementing the **SQLite** DBMS engine
Depends on: **nothing**<br>
Required by: **libspatialite**, **librasterlite**<br>

Building under Windows is an easy task.

*   download the latest sources: [sqlite-autoconf-3200100.tar.gz](http://www.sqlite.org/2017/sqlite-autoconf-3200100.tar.gz)
*   uncompress this gzipped-file
*   then uncompress the _tarball_
*   then open an MSYS2 shell (**mingw64.exe**)

**cd sqlite-autoconf-3200100**<br>
**export "CFLAGS=-DSQLITE_ENABLE_STAT3=1 -DSQLITE_ENABLE_TREE_EXPLAIN=1 -DSQLITE_ENABLE_UPDATE_DELETE_LIMIT=1 \
        -DSQLITE_ENABLE_FTS3_PARENTHESIS=1 -DSQLITE_ENABLE_COLUMN_METADATA=1 -DSQLITE_THREADSAFE=1"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="open-ssl">Step 14) building OpenSSL</a>

**OpenSSL** is a well known standard library supporting **SSL**, i.e. the **encrypted HTTPS** web protocol.
Depends on: **nothing**<br>
Required by: **libcurl**<br>

Building under Windows is a little bit difficult, and requires to pay close attention.
The **configure** script isn't at all a standard one: please read carefully the following instructions.

*   download the latest sources: [openssl-1.1.0f.tar.gz](https://www.openssl.org/source/openssl-1.1.0f.tar.gz)
*   <u>Important notice:</u> you cannot use tools such as **7z** to untar the _tarball_: this will cause fatal errors during compilation (_broken links_).
    You absolutely have to run all the following commands from the MSYS2 shell.

**tar zxvf openssl-1.1.0f.tar.gz**<br>
**cd openssl-1.1.0f**<br>
**./Configure mingw64 --prefix=/mingw64/local shared**<br>
**make**<br>
**make install**<br>

This will build and install both the _static libraries_ and the DLLs as well.

* * *

### <a name="postgres">Step 15) building PostgreSQL</a>

**PostgreSQL** is a well known enterprise DBMS; building PostgreSQL seems to be a really odd dependency for SpatiaLite (and really is).
Anyway it's required by **GDAL/OGR**; if you aren't interested in supporting GDAL/OGR you can directy jump to [libxml2](#libxml2)

Depends on: **libz**, **OpenSSL**<br>
Required by: **GDAL/OGR**<br>
Building under Windows is an easy task.

*   download the latest sources: [postgresql-9.6.5.tar.bz2](http://ftp.postgresql.org/pub/source/v9.6.5/postgresql-9.6.5.tar.bz2)
*   uncompress this bzip2-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd postgresql-9.6.5**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libxml2">Step 16) building libxml2</a>

**libxml2** is the well known XML parser of Gnome. it's an optional dependency for **libspatialite**.
If you aren't interested in supporting libxml2 you can directy jump to [libspatialite](#libspatialite)

Depends on: **libz**, **libiconv**<br>
Required by: **libspatialite**<br>
Building under Windows is an easy task.

*   download the latest sources: [libxml2-2.9.6.tar.gz](ftp://xmlsoft.org/libxml2/libxml2-2.9.6.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd libxml2-2.9.6**<br>
**./configure --prefix=/mingw64/local --without-python --with-zlib=/mingw64/local --with-lzma=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="librttopo">Step 17) building librttopo</a>

**librttopo** is a new library implementing several usefull geometry functions and fully supporting ISO-Topology operators.
If you aren't interested in supporting librttopo you can directy jump to [libspatialite](#libspatialite)

Depends on: **libgeos**<br>
Required by: **libspatialite** (_optional dependency_)
Building under Windows is an easy task.

*   download 2the latest sources: [librttopo-1.0.0.tar.gz](http://download.osgeo.org/librttopo/src/librttopo-1.0.0.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd librttopo-1.0.0**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local --with-geosconfig=/mingw64/local/bin/geos-config**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libspatialite">Step 18) building libspatialite</a>

**libspatialite** is the main core of **SpatiaLite**<br>
Depends on: **libiconv**, **libproj**, **libgeos**, **FreeXL**<br>
Required by: **spatialite-tools**, **librasterlite2**, **spatialite-gui**, **LibreWMS**, **DataSeltzer**<br>

Building under Windows is an easy task.

*   download the latest sources: [libspatialite-4.4.0-RC1.zip](http://www.gaia-gis.it/gaia-sins/libspatialite-sources/libspatialite-4.4.0-RC1.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd libspatialite-4.4.0-RC1**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>
**./configure --prefix=/mingw64/local --with-geosconfig=/mingw64/local/bin/geos-config --target=mingw32 --enable-rttopo=yes --enable-gcp=yes**<br>
**make**<br>
**make install-strip**<br>

<u>Please note</u>: the above example assumes that **librttopo** is available; if this is not your case you should obviously omit to specify the corresponding build option.

This will build and install both the _static library_ and the DLL as well.

<u>Please note</u>: if you wish, you can now execute the complete test coverage in order to verify if your build was fully succesful.
This further step is <u>optional</u> and requires a small extra time, but it's strongly suggested anyway.

**make check**<br>

* * *

### <a name="spatialite-tools">Step 19) building spatialite-tools</a>

**spatialite-tools** the **SpatiaLite** _command-line_ management tools
Depends on: **libiconv**, **libproj**, **libgeos**, **FreeXL**, **ReadOSM**, **libspatialite**, **libexpat**<br>
Building under Windows is an easy task.

*   download the latest sources: [spatialite-tools-4.4.0-RC1.zip](http://www.gaia-gis.it/gaia-sins/spatialite-tools-sources/spatialite-tools-4.4.0-RC1.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd spatialite-tools-4.4.0-RC1**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>
**./configure --prefix=/mingw64/local --with-geosconfig=/mingw64/local/bin/geos-config --target=mingw32**<br>
**make**<br>
**make install-strip**<br>

<u>Please note</u>: following the above method you'll get _dynamically linked_ tools [i.e. depending on DLLs].
If you wish instead to build _statically linked_ tools [i.e. self contained, not depending on DLLs], now type:

**mkdir static_bin**<br>
**make -f Makefile-static-mingw64**<br>
**cp static_bin/* /mingw64/local/bin**<br>

* * *

### <a name="wxWidgets">Step 20) building wxWidgets MSW</a>

**wxWidgets** is a popular _widgets_ library, supporting GUI in a cross-platform fashion; **MSW** is the specific porting supporting Windows.
Depends on: **nothing**<br>
Required by: **spatialite-gui**, **spatialite-gis**<br>

This library really is an <u>huge and complex</u> piece of software; building on Windows is an incredibly time consuming task, but is quite plain and easy.

*   download the latest sources: [wxWidgets-3.0.3.7z](https://github.com/wxWidgets/wxWidgets/releases/download/v3.0.3/wxWidgets-3.0.3.7z)
*   uncompress this 7zipped-file
*   then untar the _tarball_
*   then open an MSYS2 shell (**mingw64.exe**)

**cd wxWidgets-3.0.3**<br>
**mkdir msw_build**<br>
**cd msw_build**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "CXXFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**../configure --prefix=/mingw64/local --disable-shared --disable-debug \
    --disable-threads --enable-monolithic --enable-unicode \
    --without-regex --enable-graphics_ctx**<br>

<u>Please note</u>: the wxWidgets ./configure is highly configurable: you must apply exactly the above settings.
Anyway, when ./configure stops, it's a good practice to check if the final report looks exactly like this:

```
Configured wxWidgets 3.0.3 for `x86_64-w64-mingw32'

  Which GUI toolkit should wxWidgets use?                 msw
  Should wxWidgets be compiled into single library?       yes
  Should wxWidgets be linked as a shared library?         no
  Should wxWidgets be compiled in Unicode mode?           yes (using wchar_t)
  What level of wxWidgets compatibility should be enabled?
                                       wxWidgets 2.6      no
                                       wxWidgets 2.8      yes
  Which libraries should wxWidgets use?
                                       STL                no
                                       jpeg               sys
                                       png                sys
                                       regex              no
                                       tiff               sys
                                       zlib               sys
                                       expat              sys
                                       libmspack          no
                                       sdl                no
```

now, when ./configure stops, you have to continue as usual:

**make**<br>
**make install-strip**<br>

<u>Important notice</u>: **make install-strip** will probably raise some error, but you could safely ignore it at all.

* * *

### <a name="libfreetype">Step 21) building libfreetype</a>

**libfreetype** is a standard library supporting **TrueType** fonts.
Depends on: **nothing**<br>
Required by: **libcairo**, ...

Building under Windows is an easy task.

*   download the latest sources: [freetype-2.8.1.tar.gz](http://download.savannah.gnu.org/releases/freetype/freetype-2.8.1.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd freetype-2.8.1**<br>
**./configure --prefix=/mingw64/local --with-bzip2=no**<br>
**make**<br>
**make install**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libfontconfig">Step 22) building libfontconfig</a>

**libfontconfig** is a standard library supporting **font customization** and configuration.
Depends on: **libexpat**, **libfreetype**, **libiconv**<br>
Required by: **libcairo**, ...

Building under Windows is an easy task.

*   download the latest sources: [fontconfig-2.12.6.tar.gz](https://www.freedesktop.org/software/fontconfig/release/fontconfig-2.12.6.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd fontconfig-2.12.6**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>
**./configure --prefix=/mingw64/local --disable-docs --enable-shared --enable-static**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libpixman">Step 23) building libpixman</a>

**libpixman** is the standard library implementing **pixel manipulation** for Cairo.
Depends on: **nothing**<br>
Required by: **libcairo**, ...

Building under Windows is an easy task.

*   download the latest sources: [pixman-0.34.0.tar.gz](http://cairographics.org/releases/pixman-0.34.0.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd pixman-0.34.0**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libcairo">Step 24) building libcairo</a>

**libcairo** is a very popular **graphics** library.
Depends on: **libpixman**, **libfreetype**, **libfontconfig**, **libpng**<br>
Required by: **librasterlite2**, ...

Building under Windows is a little bit harder than usual.

*   download the latest sources: [cairo-1.14.10.tar.xz](http://cairographics.org/releases/cairo-1.14.10.tar.xz)
*   uncompress this XZ-compressed file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd cairo-1.14.10**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="giflib">Step 25) building giflib</a>

**giflib** is a popular library supporting the **GIF** image compression.
Depends on: **nothing**<br>
Required by: **librasterlite2**, ...

Building under Windows is absolutely a plain and easy task.

*   download the latest sources: [giflib-5.1.4.tar.gz](http://sourceforge.net/projects/giflib/files/giflib-5.1.4.tar.gz/download)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd giflib-5.1.4**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libwebp">Step 26) building libwebp</a>

**libwebp** is an innovative library supporting the recently introduced **WebP** image compression.
Depends on: **libjpeg**, **libpng**, **libtiff**<br>
Required by: **librasterlite2**, ...

Building under Windows is absolutely a plain and easy task.

*   download the latest sources: [libwebp-0.6.0.tar.gz](http://downloads.webmproject.org/releases/webp/libwebp-0.6.0.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd libwebp-0.6.0**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="CharLS">Step 27) building CharLS</a>

**CharLS** is an innovative library supporting the **JPEG-LS** image compressor; don't be fooled by the name, JPEG-LS is not JPEG. It's based on a completely different algorithm and it supports genuine _lossless_ compression.
Depends on: **nothing**<br>
Required by: **librasterlite2**, ...

Building under Windows is absolutely a plain and easy task; anyway you'll surely encounter many annoying complications if you'll try to directly build CharLS starting from the standard sources available from [here](http://charls.codeplex.com/downloads/get/165715) because the code is not really portable (it's obviously intended for the MVSC compiler alone) and it lacks an effective build script adequately supporting MinGW64 and MSYS2.
Accordingly to all this I've duly repackaged **CharLS 1.0** by applying few patches resolving any **gcc**-related issue and fully supporting a canonical **./configure** script.

*   download the already patched sources: [charls-1.0.zip](http://gaia-gis.it/gaia-sins/CharLS/charls-1.0.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd charls-1.0**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="lcms2">Step 28) building lcms2</a>

**lcms2** is an innovative library implementing a Color Management Engine.
Depends on: **libjpeg**, **libpng**, **libtiff**<br>
Required by: **OpenJpeg-2**<br>

Building under Windows is absolutely a plain and easy task.

*   download the latest sources: [lcms2-2.8.zip](http://sourceforge.net/projects/lcms/files/lcms/2.8/lcms2-2.8.zip/download)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd lcms2-2.8**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --prefix=/mingw64/local**<br>
**make**<br>
**make install-strip**<br>

<u>Important notice</u>: you have to properly set the shell environment in order to retrieve the already installed **libz**; this is the duty of the two above <u>export</u> directives.
This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="OpenJpeg">Step 29) building OpenJpeg-2</a>

**OpenJpeg-2** is an open source library supporting **Jpeg2000** compressed images.
Depends on: **libcms2**, **libpng**, **libtiff**<br>
Required by: **librasterlite2**<br>

Building under Windows is absolutely a plain and easy task.
It only has the rather unusual characteristics to adopt **CMake**-based build scripts, so you'll probably be required to install CMake before attempting to build OpenJpeg.
Anyway you can easily download the most recent CMake Windows Installer from [here](http://www.cmake.org/download/)

*   download the latest sources: [openjpeg-2.3.0.zip](https://github.com/uclouvain/openjpeg/archive/v2.3.0.zip)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

<u>Important notice</u>: you absolutely have to apply the following patch before attempting to build OpenJpeg.
The intended scope is the one to redefine the DLL's specific prefix accordingly to MinGW64 requirements. So manually edit the **-/src/lib/openjp2/openjpeg.h** source:

```
near line 94:
----------------------------------------
   #define OPJ_API
   #define OPJ_LOCAL
   #endif
   #define OPJ_CALLCONV
< #else
< #define OPJ_CALLCONV __stdcall
> #else
> #if defined (__MINGW32__)
> #define OPJ_CALLCONV
> #else
> #define OPJ_CALLCONV __stdcall
> #endif
```

then you should continue to apply this second patch into the same file:

```
near line 110:
----------------------------------------
   #if defined(OPJ_EXPORTS) || defined(DLL_EXPORT)
   #define OPJ_API __declspec(dllexport)
   #else
< #define OPJ_API __declspec(dllimport)
> #if defined (__MINGW32__)
> #define OPJ_API extern
> #else
> #define OPJ_API __declspec(dllimport)
> #endif /* MinGW64 */
```

**cd openjpeg-2.3.0**<br>
**mkdir build_dll**<br>
**cd build_dll**<br>
**cmake -G "MSYS Makefiles" -DCMAKE_INSTALL_PREFIX=/mingw64/local \
-DBUILD_PKGCONFIG_FILES=ON \
-DZLIB_INCLUDE_DIR=/mingw64/local/include \
-DZLIB_LIBRARY=/mingw64/local/lib/libz.dll.a \
-DPNG_PNG_INCLUDE_DIR=/mingw64/local/include/libpng16 \
-DPNG_LIBRARY=/mingw64/local/lib/libpng16.dll.a \
-DTIFF_INCLUDE_DIR=/mingw64/local/include \
-DTIFF_LIBRARY=/mingw64/local/lib/libtiff.dll.a \
-DLCMS2_INCLUDE_DIR=/mingw64/local/include \
-DLCMS2_LIBRARY=/mingw64/local/lib/liblcms2.dll.a ..**<br>
**make**<br>
**make install/strip**<br>

<u>Please note</u>: this will simply build and install the DLL. Now you must build the _static library_ as follows:

**cd ..**<br>
**mkdir build_static**<br>
**cd build_static**<br>
**cmake -G "MSYS Makefiles" -DCMAKE_INSTALL_PREFIX=/mingw64/local \
-DBUILD_SHARED_LIBS=OFF \
-DZLIB_INCLUDE_DIR=/mingw64/local/include \
-DZLIB_LIBRARY=/mingw64/local/lib/libz.dll.a \
-DPNG_PNG_INCLUDE_DIR=/mingw64/local/include/libpng16 \
-DPNG_LIBRARY=/mingw64/local/lib/libpng16.dll.a \
-DTIFF_INCLUDE_DIR=/mingw64/local/include \
-DTIFF_LIBRARY=/mingw64/local/lib/libtiff.dll.a \
-DLCMS2_INCLUDE_DIR=/mingw64/local/include \
-DLCMS2_LIBRARY=/mingw64/local/lib/liblcms2.dll.a ..**<br>
**make openjp2**<br>
**cp bin/libopenjp2.a /mingw64/local/lib**<br>

All done: now you've built and installed both the _static library_ and the DLL as well.

* * *

### <a name="libcurl">Step 30) building libcurl</a>

**libcurl** is a well known library supporting **URL**s (networking, web protocols)
Depends on: **libz**, **OpenSSL**<br>
Required by: **librasterlite2**, ...

Building under Windows is an easy task.

*   download the latest sources: [curl-7.56.0.zip](http://curl.haxx.se/download/curl-7.56.0.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd curl-7.56.0**<br>
**export "CPPFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>
**./configure --prefix=/mingw64/local --enable-shared=no --with-zlib=/mingw64/local \
        --with-libssh2=no --with-gssapi=no --disable-tls-srp --disable-rtsp --with-librtmp=no**<br>
**make**<br>
**make install-strip**<br>

This will build and install only the _static library_; so you are now required to perform a second pass in order to build the _dynamic library_ as well:

**make distclean**<br>
**./configure --prefix=/mingw64/local --enable-shared=yes --with-zlib=/mingw64/local \
        --with-libssh2=no --with-gssapi=no --disable-tls-srp --disable-rtsp --with-librtmp=no**<br>
**make**<br>
**make install-strip**<br>

All right; you are now ready to use libcurl.

* * *

### <a name="librasterlite2">Step 31) building librasterlite2</a>

**librasterlite2** is a library supporting **rasters**<br>
Depends on: **libjpeg**, **libpng**, **libtiff**, **libgeotiff**, **libcairo**, **libgif**, **libwebp**<br>

Required by: **spatialite-gui**, **LibreWMS**<br>

Building under Windows is an easy task.

*   download the latest sources: [librasterlite2-1.0.0-devel.zip](http://www.gaia-gis.it/gaia-sins/librasterlite2-sources/librasterlite2-1.0.0-devel.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd librasterlite2-1.0.0-devel**<br>
**export "CFLAGS=-I/mingw64/local/include -DCURL_STATICLIB"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>
**./configure --prefix=/mingw64/local --target=mingw32**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="spatialite-gui">Step 32) building spatialite_gui</a>

**spatialite_gui** the **SpatiaLite** _GUI_ user-friendly tool
Depends on: **libspatialite**, **wxWidgets**, **librasterlite2**<br>
Building under Windows is an easy task.

*   download the latest sources: [spatialite_gui-2.0.0-devel.zip](http://www.gaia-gis.it/gaia-sins/spatialite-gui-sources/spatialite_gui-2.0.0-devel.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

**cd spatialite_gui-2.0.0-devel**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>
**./configure --prefix=/mingw64/local --with-wxconfig=/mingw64/local/bin/wx-config \
        --with-geosconfig=/mingw64/local/bin/geos-config**<br>
**make**<br>
**make install-strip**<br>

<u>Please note</u>: following the above method you'll get a _dynamically linked_ GUI tool [i.e. depending on DLLs].
If you wish instead to build a _statically linked_ GUI tool [i.e. self contained, not depending on DLLs], now type:

**mkdir static_bin**<br>
**make -f Makefile-static-mingw64**<br>
**cp static_bin/* /mingw64/local/bin**<br>

* * *

### <a name="librewms">Step 33) building LibreWMS</a>

**LibreWMS** is an open source WMS Viewer built on the top of **SpatiaLite** and **RasterLite2**.
Building under Windows is an easy task.

*   download the latest sources: [librewms-1.0.0.zip](http://www.gaia-gis.it/gaia-sins/librewms-1.0.0a.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

First of all, you must check if you've already installed **pkg-config.exe**<br>
If not, please read the above [instructions](#pkg-config)

And now you must set the **PKG_CONFIG_PATH** as appropriate:

**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>

After this you are now ready to build as usual:

**cd librewms-1.0.0a**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --target=mingw32**<br>
**make**<br>
**make install-strip**<br>

<u>Please note</u>: following the above method you'll get a _dynamically linked_ GUI tool [i.e. depending on DLLs].
If you wish instead to build a _statically linked_ app [i.e. self contained, not depending on DLLs], now type:

**mkdir static_bin**<br>
**make -f Makefile-static-MinGW64**<br>
**cp static_bin/* /mingw64/local/bin**<br>

* * *

### <a name="minizip">Step 34) building minizip</a>

**minizip** is an innovative library supporting direct creation of compressed **ZipFile** archives.
Depends on: **libz**<br>
Required by: **DataSeltzer**, ...

Building under Windows is absolutely a plain and easy task; anyway there is a little preliminary complication.
The **MiniZIP** library isn't directly distributed as such; it simply is an auxiliary package you'll find within any standard source tarball distribution of **zlib** (on the **contribs** folder).
Unhappily the original distribution is a little bit dirty / unfinished, and lacks any accompanying **./configure** script (strictly required by MinGW64/MSYS2).
So there are two possible alternatives to circumvent this issue:

1.  build first **MiniZIP** on some Linux platform. Then create a standard tarball distribution by calling **make dist** And finally copy this tarball to your Windows platform.
2.  directly download this pre-configured source tarball: [minizip-1.2.8.tar.gz](http://www.gaia-gis.it/gaia-sins/dataseltzer-sources/minizip-1.2.8.tar.gz)

Once you've got the source tarball you can continue following the usual approach:

*   untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd minizip-1.2.8**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure**<br>
**make**<br>
**make install-strip**<br>

<u>Important notice</u>: you have to properly set the shell environment in order to retrieve the already installed **libz**; this is the duty of the two above <u>export</u> directives.
This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="libcgi">Step 35) building libcgi</a>

**libcgi** is an standard library supporting the **CGI** environment.
Depends on: **nothing**<br>
Required by: **DataSeltzer**<br>

Building under Windows is absolutely a plain and easy task.

*   download the latest sources: [libcgi-1.0.tar.gz](http://sourceforge.net/projects/libcgi/files/libcgi/1.0/libcgi-1.0.tar.gz/download)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd libcgi-1.0**<br>
**./configure**<br>
**make**<br>
**make install**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="dataseltzer">Step 36) building DataSeltzer CGI</a>

**DataSeltzer** is a CGI component supporting OpenData dissemination built on the top of **SpatiaLite**.
Building under Windows is an easy task.

*   download the latest sources: [dataseltzer-1.0.0.zip](http://www.gaia-gis.it/gaia-sins/dataseltzer-1.0.0.zip)
*   uncompress this zip-file
*   then open an MSYS2 shell (**mingw64.exe**)

First of all, you must check if you've already installed **pkg-config.exe**<br>
If not, please read the above [instructions](#pkg-config)

And now you must set the **PKG_CONFIG_PATH** as appropriate:

**export "PKG_CONFIG_PATH=/mingw64/local/lib/pkgconfig"**<br>

After this you are now ready to build as usual:

**cd dataseltzer-1.0.0**<br>
**export "CFLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --target=mingw32**<br>
**make**<br>
<u>Please note</u>: following the above method you'll get a _dynamically linked_ CGI component [i.e. depending on DLLs].
If you wish instead to build a _statically linked_ CGI [i.e. self contained, not depending on DLLs], now type:

**mkdir static_bin**<br>
**make -f Makefile-static-MinGW64**<br>

<u>Please note</u>: installing a CGI component requires special precautions depending on the specific Web Server configuration.

* * *

### <a name="gdal">Step 37) building GDAL</a>

**GDAL** is the well known Geospatial Data Abstraction library. it's strictly required by **mod_gdalsplite**.
Depends on: practically all libraries built since now
Required by: **mod_gdalsplite**<br>
Building under Windows is a very long but easy task.

*   download the latest sources: [gdal201.zip](http://download.osgeo.org/gdal/2.0.1/gdal201.zip)
*   uncompress this zipped-file
*   and finally open an MSYS2 shell (**ming64.exe**)

**cd gdal-2.0.1**<br>
**export "CPPLAGS=-I/mingw64/local/include"**<br>
**export "LDFLAGS=-L/mingw64/local/lib"**<br>
**./configure --with-liblzma=yes --with-spatialite=yes --with-webp=yes**<br>
**make**<br>
**make install-strip**<br>

This will build and install both the _static library_ and the DLL as well.

* * *

### <a name="fossil">Step 38) building fossil</a>

**fossil** is the SCM [_Source Code Management_] used by SpatiaLite (and companion) projects.
Building on Windows is an easy and plain task.

*   download the latest sources: [fossil-src-20130216000435.tar.gz](http://www.fossil-scm.org/download/fossil-src-20130216000435.tar.gz)
*   uncompress this gzipped-file
*   then untar the _tarball_
*   and finally open an MSYS2 shell (**ming64.exe**)

<u>Important notice</u>: you are required to manually edit the **-/win/Makefile.mingw** script as follows:

<table bgcolor="#ffffe0">

<tbody>

<tr>

<td>**Line 55**: replace _# FOSSIL_ENABLE_SSL = 1_ as: **FOSSIL_ENABLE_SSL = 1**</td>

</tr>

<tr>

<td>**Line 81**: replace _ZINCDIR = $(SRCDIR)/../zlib-1.2.6_ as: **ZINCDIR = /mingw64/local/include**</td>

</tr>

<tr>

<td>**Line 82**: replace _ZLIBDIR = $(SRCDIR)/../zlib-1.2.6_ as: **ZLIBDIR = /mingw64/local/lib**</td>

</tr>

<tr>

<td>**Line 89**: replace _OPENSSLINCDIR = $(SRCDIR)/../openssl-1.0.0g/include_ as: **OPENSSLINCDIR = /mingw64/local/include**</td>

</tr>

<tr>

<td>**Line 90**: replace _OPENSSLLIBDIR = $(SRCDIR)/../openssl-1.0.0g_ as: **OPENSSLLIBDIR = /mingw64/local/lib**</td>

</tr>

<tr>

<td>**Line 661,662**: delete both lines
_zlib:_
_    $(MAKE) -C $(ZLIBDIR) PREFIX=$(PREFIX) -f win32/Makefile.gcc libz.a_</td>

</tr>

<tr>

<td>**Line 663**: delete _zlib_ at the end of the line.</td>

</tr>

</tbody>

</table>

After this you are now ready to build:

**cd fossil-src-20130216000435**<br>
**make -f win/Makefile.mingw**<br>
**strip --strip-all fossil.exe**<br>

* * *

Back to the [Gaia-SINS](http://www.gaia-gis.it/gaia-sins/index.html) home page

<table width="100%" bgcolor="#ddffdd">

<tbody>

<tr>

<th>last updated: 2017-10-07</th>

</tr>

</tbody>

</table>